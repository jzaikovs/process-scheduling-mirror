# CPU procesu plānošana algoritmu vizualizācija

Vizualizācija veidota iekš HTML, CSS un JavaScript (TypeScript).

Biznesa loģija rakstīta iekš TypeScript `/js/cpu.ts` fails.

3 pamata klases:
* Process - klase, kas glabā informāciju par procesu.
* CPU - kontroles klase, simulē procesa izpildi, pātraukumus un izsauc plānotāju, lai iegūtu nākošo izpildāmo procesu.
* Scheduler - abstrakta klase, kas implemēntē pamata plānošanas funkcijas un glabā izpildāmo procesu rindu.

Katram plānošanas algoritmam ir sava klase, kas ir atvasināta no Scheduler klases, kas pārslogo `SelectNext()` metodi.

Prezentācijas loģija veidota iekš JavaScript `/js/main.js` fails.

3 pamata klases
* Animation - kontroles klase, kontrolē animācijas izpildi.
* Queue - kontroles klase, kontrolē procesu tabulas animācijas, jaunu procesu pievienošanu, dzēšanu un labošanu.
* Chart - kontroles klase, kontrolē izpildes grafika zīmēšanu.

# Process Scheduling

## First Come First Served
Procesi tiek izpildīti kārtībā, kā tie ienāk.
Nākošais process tiek izvēlēts tikai tad, kad iepriekšējais ir pabeigts.

## Shortest Job First
Procesi ar visīsāko paredzēto izpildes laiku tiek izpildīti vispirms.
Nākošais process tiek izvēlēts tikai tad, kad iepriekšējais ir pabeigts.

### Pārtraucošais (SRTF)
Atvasinājums no SJF, iekļaujot pārtraukumus. Pātraukums notiek, ja rindā ienāk process, kura paredzētais izpildes laiks 
ir mazāks nekā atlicis procesam, kuru izpilda.
Pārbaude jāveic katru takti, lai pārbaudītu, vai nav ienācis jauns uzdevums ar īsāku izpildes laiku.
Ja ir ienācis jauns process ar īsaku izpildi, tad pārtrauc iesākto un pilda ienākošo.
Procesa izpildi pabeidzot, tiek izvēlēts nākošais process no rindas ar īsāko izpildes laiku.

## Round Robin
Procesi tiek izpildīti kārtībā, kādā tie ienāk.
Procesu izpilde tiek pārslēgta uz nakošo rindā pēc noteikta laika momenta Q.
Pārtrauktais process tiek pārvietots uz rindas beigām.

## Priority
Izpilda ienākošos uzdevumus pēc prioritātes (augstāka prioritāte ir ar mazāko prioritātes vērtību).
Ja ienāk jauns uzdevums ar augstāku prioritāti, ieksākto nepārtrauc, izpilda līdz galam un tad pilda nākošo,
kuram ir augstāka prioritāte.

### Pārtraucošais
Atvasinājums no Priority, iekļaujot pārtraukums. Izpilda ienākošos uzdevumus pēc prioritātes, ja ienāk uzdevums ar augstāku 
prioritāti, iesāktais uzdevums tiek pārtraukts, tiek pildīts augstākās prioritātes uzdevums.
Pārbaude vajadzīga katrā taktī, vai nav ienācis uzdevums ar augstāku prioritāti.

