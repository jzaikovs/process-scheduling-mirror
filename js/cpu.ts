﻿
var PS_SCHEDULED = "Ieplānots"
var PS_CREATED = "Izveidots"
var PS_WAITING = "Gaida";
var PS_EXECUTING = "Izpilda";
var PS_DONE = "Pabeigts";

/* Procesa reprezentējoša objekta klase */
class Process {
	name : string;  // procesa nosaukums
	status : string; // procesa status no PS_* konstantēm
	
	id : number; // procesa Id, jabūt unikālam
	cpu : number; // atlikušais izpildes laiks
	place : number;
	priority : number;	
    waittime : number;
    waiting_from : number;
    initial : number; // sākotnējais izpildes laiks
	atime : number; // processa ienākšanas laiks
	
	constructor (name : string, cpu : number, at : number, priority : number) {		
		this.name = name;
		this.status = PS_SCHEDULED;

		this.atime = at;
		this.cpu = cpu;
		this.initial = cpu;
		this.priority = priority;		
        this.waittime = 0;
        this.waiting_from = at;
	}

	Clone() : Process {
		return new Process(this.name, this.initial, this.atime, this.priority);
	}	
}

/* Klase reprezentē procesoru */
class CPU {
	tick : number;
	estimate : number;
	canInterup : bool;

	scheduler : Scheduler;
	executing : Process; // process kuru izpildijām

	constructor () {
		this.tick = 0;
		this.estimate = 0;
		this.canInterup = false;
	}	  

	/* Metode CPU takts izpildei */
	Tick() : Process {
		if (this.scheduler == null || !this.scheduler.HasJob()) {
			this.tick++;
			return null;            
		}

 		// ja CPU atbalsta pārtraukumus tad pieprasam plānotājam jaunu procesu
		if (this.canInterup || this.executing == null || this.executing.status == PS_DONE) {
			if (this.executing != null && this.executing.status != PS_DONE) {
				this.executing.status = PS_WAITING;		    
				this.executing.waiting_from = this.tick;    
			}

			this.executing = this.scheduler.SelectNext();			            
			if (this.executing == null) {
				this.tick++;
				return null;
			}
			this.executing.status = PS_EXECUTING;    	
			this.executing.waittime += this.tick - this.executing.waiting_from; // izrēķinam gaidīšanas laiku no momenta kad sācis gaidīt            
		}

		this.executing.cpu--;		
		this.tick++;         

		if (this.executing.cpu <= 0) {            
			// Process ir izpildīts, varam noņemt no rindas
			this.executing.status = PS_DONE;
			this.scheduler.Remove(this.executing);
		}	

		return this.executing;
	}
}

/* Klase reprezentē plānotāju */
class Scheduler {
    cpu : CPU;     
    queue : Process[];       
    
    constructor (cpu : CPU){
        this.queue = [];        
        this.cpu = cpu;        
    }
    
	/* Metode pēc ar kuru izvēlās nākošo procesu izpildei */
	SelectNext() : Process {
        return null;
	}

	HasJob() : bool {
		return this.queue.length > 0;
	}
        
    /*
        Metode ar kuru pievieno rindā procesusu
        ! Daži plānotāji var veikt rindas optimizāciju, lai vieglāk izvēlēties nākošo procesu
    */
    Push(process : Process) : bool {
        if (process.atime > this.cpu.tick)
        	 return false;
		
		process.place = this.queue.length;
		process.waiting_from = this.cpu.tick;
		process.status = PS_WAITING;

		this.cpu.estimate += process.cpu;            
		this.queue.push(process);

		return true;
    }
    
    /* Metode lai no procesu rindas noņemtu procesu */
    Remove(process : Process) {
        this.queue.splice(process.place, 1);
		for (var i = process.place; i < this.queue.length; i++) {
			this.queue[i].place--;
		}		
    }    
}

class FirstComeFirstServe extends Scheduler {

	SelectNext() : Process {
		return this.queue[0];
	}
}

class ShortestJobFirst extends Scheduler {

	SelectNext() : Process {
		var a = this.queue[0];

		for (var i in this.queue) 
			if (a.cpu > this.queue[i].cpu)
				a = this.queue[i];

		return a;
	}
}

class Priority extends Scheduler {

    SelectNext() : Process {
		var a = this.queue[0];

		for (var i in this.queue) 
			if (a.priority > this.queue[i].priority)
				a = this.queue[i];

		return a;
	}
}

class RoundRobin extends Scheduler {
	private q : number;
	private count : number;
	private active : Process;
	
	constructor(cpu : CPU, q : number) {        
        super(cpu);        
        cpu.canInterup = true;        
		this.q = q;
		this.count = 0;
		this.active = null;
	}
		
	SelectNext() : Process {
		// select first if first time
		if (this.active == null) {
			this.active = this.queue[0];									
		}
		
		if (this.active.status == PS_DONE) {
			this.count = 0;
			this.active = this.queue[(this.active.place) % this.queue.length];				
		}		
		
		if (this.count >= this.q) {
			this.count = 0;
			this.active = this.queue[(this.active.place + 1) % this.queue.length];				
		}		
		
		this.count++;
		return this.active;		
	}
}

