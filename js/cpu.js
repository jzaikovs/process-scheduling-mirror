﻿var __extends = this.__extends || function (d, b) {
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var PS_SCHEDULED = "Ieplānots";
var PS_CREATED = "Izveidots";
var PS_WAITING = "Gaida";
var PS_EXECUTING = "Izpilda";
var PS_DONE = "Pabeigts";
var Process = (function () {
    function Process(name, cpu, at, priority) {
        this.name = name;
        this.status = PS_SCHEDULED;
        this.atime = at;
        this.cpu = cpu;
        this.initial = cpu;
        this.priority = priority;
        this.waittime = 0;
        this.waiting_from = at;
    }
    Process.prototype.Clone = function () {
        return new Process(this.name, this.initial, this.atime, this.priority);
    };
    return Process;
})();
var CPU = (function () {
    function CPU() {
        this.tick = 0;
        this.estimate = 0;
        this.canInterup = false;
    }
    CPU.prototype.Tick = function () {
        if(this.scheduler == null || !this.scheduler.HasJob()) {
            this.tick++;
            return null;
        }
        if(this.canInterup || this.executing == null || this.executing.status == PS_DONE) {
            if(this.executing != null && this.executing.status != PS_DONE) {
                this.executing.status = PS_WAITING;
                this.executing.waiting_from = this.tick;
            }
            this.executing = this.scheduler.SelectNext();
            if(this.executing == null) {
                this.tick++;
                return null;
            }
            this.executing.status = PS_EXECUTING;
            this.executing.waittime += this.tick - this.executing.waiting_from;
        }
        this.executing.cpu--;
        this.tick++;
        if(this.executing.cpu <= 0) {
            this.executing.status = PS_DONE;
            this.scheduler.Remove(this.executing);
        }
        return this.executing;
    };
    return CPU;
})();
var Scheduler = (function () {
    function Scheduler(cpu) {
        this.queue = [];
        this.cpu = cpu;
    }
    Scheduler.prototype.SelectNext = function () {
        return null;
    };
    Scheduler.prototype.HasJob = function () {
        return this.queue.length > 0;
    };
    Scheduler.prototype.Push = function (process) {
        if(process.atime > this.cpu.tick) {
            return false;
        }
        process.place = this.queue.length;
        process.waiting_from = this.cpu.tick;
        process.status = PS_WAITING;
        this.cpu.estimate += process.cpu;
        this.queue.push(process);
        return true;
    };
    Scheduler.prototype.Remove = function (process) {
        this.queue.splice(process.place, 1);
        for(var i = process.place; i < this.queue.length; i++) {
            this.queue[i].place--;
        }
    };
    return Scheduler;
})();
var FirstComeFirstServe = (function (_super) {
    __extends(FirstComeFirstServe, _super);
    function FirstComeFirstServe() {
        _super.apply(this, arguments);

    }
    FirstComeFirstServe.prototype.SelectNext = function () {
        return this.queue[0];
    };
    return FirstComeFirstServe;
})(Scheduler);
var ShortestJobFirst = (function (_super) {
    __extends(ShortestJobFirst, _super);
    function ShortestJobFirst() {
        _super.apply(this, arguments);

    }
    ShortestJobFirst.prototype.SelectNext = function () {
        var a = this.queue[0];
        for(var i in this.queue) {
            if(a.cpu > this.queue[i].cpu) {
                a = this.queue[i];
            }
        }
        return a;
    };
    return ShortestJobFirst;
})(Scheduler);
var Priority = (function (_super) {
    __extends(Priority, _super);
    function Priority() {
        _super.apply(this, arguments);

    }
    Priority.prototype.SelectNext = function () {
        var a = this.queue[0];
        for(var i in this.queue) {
            if(a.priority > this.queue[i].priority) {
                a = this.queue[i];
            }
        }
        return a;
    };
    return Priority;
})(Scheduler);
var RoundRobin = (function (_super) {
    __extends(RoundRobin, _super);
    function RoundRobin(cpu, q) {
        _super.call(this, cpu);
        cpu.canInterup = true;
        this.q = q;
        this.count = 0;
        this.active = null;
    }
    RoundRobin.prototype.SelectNext = function () {
        if(this.active == null) {
            this.active = this.queue[0];
        }
        if(this.active.status == PS_DONE) {
            this.count = 0;
            this.active = this.queue[(this.active.place) % this.queue.length];
        }
        if(this.count >= this.q) {
            this.count = 0;
            this.active = this.queue[(this.active.place + 1) % this.queue.length];
        }
        this.count++;
        return this.active;
    };
    return RoundRobin;
})(Scheduler);
