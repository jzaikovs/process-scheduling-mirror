

var animation;

/* inicializācijas funkcija */
function init(){	

	animation = new Animation();	

	//bootstrap hacks, lai tooltipi pārvietos, ja logam maina izmēru
	$(window).on('resize', function () {
		animation.chart.UpdateTooltips();
	});

	// pievienojam eventu pogām ar kurām pārslēdz procesu plānošanas algoritmus
	$('#mode').children().click(function(){		
		var caller = $(this);		
		console.log("Click " + caller.attr('data-mode'));
		caller.parent().children().attr('class', 'btn');

		if (caller.attr('data-mode') != 'rr')
			$('#rr-q-form').hide();
		else 
			$('#rr-q-form').show();	

		caller.addClass('btn-success');
		animation.Switch(caller.attr('data-mode'));
	});

	$('#mode').children().eq(0).click();

	animation.queue.Enqueue(new Process('1.process', 11, 0, 5));
	animation.queue.Enqueue(new Process('2.process', 1, 7, 1));
	animation.queue.Enqueue(new Process('3.process', 3, 6, 3));
	animation.queue.Enqueue(new Process('4.process', 1, 12, 4));
	animation.queue.Enqueue(new Process('5.process', 6, 4, 2));	

	animation.queue.Update();
	animation.chart.Update();
}

var Animation = (function(){
	function Animation(){
		this.chart = new Chart(this, $('#cpuChart'));
		this.queue = new Queue(this, $('#queueTable'));
		this.queue.chart = this.chart;
		this.chart.queue = this.queue;				
		this.type = '';
	}

	Animation.prototype.Reset = function(){			
		this.queue.Reset();	
		this.chart.Clear();	
		this.chart.Update();
	}

	Animation.prototype.Switch = function(type){
		this.type = type;
		this.cpu = new CPU();

		switch (type) {
		case "rr":
			var q = parseInt($('#rr-q').val(), 10);		        
			this.cpu.scheduler = new RoundRobin(this.cpu, q);		
			break;		
		case "fcfs":		
			this.cpu.scheduler = new FirstComeFirstServe(this.cpu);
			break;
		case "stf":		
			this.cpu.scheduler = new ShortestJobFirst(this.cpu);
			break;
		case "stf-i":		
			this.cpu.canInterup = true;
			this.cpu.scheduler = new ShortestJobFirst(this.cpu);            
			break;
		case "priority":        		
			this.cpu.scheduler = new Priority(this.cpu);
			break;
		case "priority-i":        
			this.cpu.canInterup = true;
			this.cpu.scheduler = new Priority(this.cpu);           
			break;
		}		

		this.Reset();
	}

	Animation.prototype.RunFrame = function() {	
		console.log("RunFrame CPU:" + this.cpu.tick);		
		var scheduleCount = 0;
		var ps = this.queue.processes;
		for (var i in ps) {
			if (ps[i].status == PS_SCHEDULED) {
				if (ps[i].atime <= this.cpu.tick) {				
					this.cpu.scheduler.Push(ps[i]);
					ps[i].status = PS_CREATED;
					this.chart.ProcessCreated(ps[i]);
				} else {
					scheduleCount++;
				}
			}
		}

		console.log("Scheduled to arrive later " + scheduleCount);

		if (!this.cpu.scheduler.HasJob() && scheduleCount == 0) {
			console.log("Frame aborted!");
			return false;
		}

		var p = this.cpu.Tick();	
		
		console.log(p);

		this.chart.ProcessExecuted(p);
		this.queue.Update();
		
		return true;
	}

	Animation.prototype.Run =  function() {
		setTimeout(function() {
			if (animation.RunFrame()) 
				animation.Run();
		}, 300);
	}

	return Animation;
})();

var Queue = (function(){
	function Queue(animation, queue){
		
		this.chart = null;		
		this.animation = animation;

		this.processes = [];
		this.estimate = 0;
		this.seq = 0;
		this.size = 0;		
		this.queue = queue;
		

		this.queue.html('');
		
	}


	// metode, lai pievienotu jaunu procesu no formas
	Queue.prototype.NewProcess = function(event, caller){
		event.preventDefault();	
		
		var name = caller.name.value;
		var cputime = parseInt(caller.cpu.value, 10);
		var atime = caller.atime.value.length == 0 ? cpu.tick : parseInt(caller.atime.value, 10);
		var priority = caller.priority.value.length == 0 ? 1 : parseInt(caller.priority.value, 10);

		this.Enqueue(new Process(name, cputime, atime, priority));		
		this.Update();							
		this.chart.AddColumn(cputime);
		this.chart.AddRow(1);
		this.chart.Update();		
	}

	Queue.prototype.Remove = function(id) {
		this.size--;
		$('tr[data-row='+id+']').remove();

		this.estimate -= this.processes[id].initial;		

		for (var i in this.processes){
			if (this.processes[i].id == id){
				delete this.processes[i];	
				break;
			}
		}
		
		this.chart.RemoveRow()
		//this.chart.Update();		

		this.animation.Switch(this.animation.type);
	}

	Queue.prototype.Reset = function (){
		this.processes = [];
		this.estimate = 0;
		this.seq = 0;
		this.size = 0;		
		queue = this;
		this.queue.children().each(function (i, child){
			child = $(child);
			
			var name = child.children().eq(1).html();
			var cputime = parseInt($('input', child).eq(1).val(), 10);
			var atime = parseInt($('input', child).eq(2).val(), 10);
			var priority = parseInt($('input', child).eq(3).val(), 10);
			var p = new Process(name, cputime, atime, priority);
			queue.Enqueue(p);
			child.children().eq(0).html(p.id);
		});
		
		this.Update();
	}

	Queue.prototype.Enqueue = function (process) {	
		console.log(process);
		this.size++;	
		process.id = this.seq++;
		this.estimate += process.initial;
		this.processes.push(process);		
	}

	Queue.prototype.CreateField = function (id, name, value) {
		var input = $('<input>');
		input.attr({type:'text', class:'input input-block tdinput'})
		input.attr('id', 'row_'+name+'_'+id);
		input.attr('data-id', id);
		input.attr('data-field', name);
		input.val(value);
		var queue = this;
		input.on('blur', function(){
			var call = $(this);
			var id = parseInt(call.attr('data-id'));
			var field = call.attr('data-field');
			var process = queue.processes[id];

			process[field] = parseInt(call.val());
		});
		return input;
	}

	Queue.prototype.Get = function(row) {
		//return $('tr[data-row='+row+']', this.queue);
		return $('tr', this.queue).eq(row);
	}

	Queue.prototype.AddRow = function(i, process){
		row = $('<tr>');
		row.attr('data-row', i);
		row.append($('<td>').html(i));
		row.append($('<td>').html(process.name));
		row.append($('<td>').addClass('align-right').append(this.CreateField(i, 'cpu', process.cpu)));
		row.append($('<td>').addClass('align-right').append(this.CreateField(i, 'initial', process.initial)));
		row.append($('<td>').addClass('align-right').append(this.CreateField(i, 'atime', process.atime)));
		row.append($('<td>').addClass('align-right').append(this.CreateField(i, 'priority', process.priority)));
		row.append($('<td>').html(process.status));
		row.append($('<td>').addClass('align-right').attr('id', 'row_waittime_'+i).html(process.waittime));
		row.append($('<td>').html("<button class=\"btn btn-danger btn-mini\" onclick=\"animation.queue.Remove(" + i + ")\">Dzēst</button>"));
		this.queue.append(row);
	}

	Queue.prototype.Update = function() {	
		console.log(this.processes)	;
		for (var i in this.processes) {
			var process = this.processes[i];
			var row = this.Get(i);		

			if (row.length > 0) {
				//samainam klasi attiecīgi
				//row = ;
				switch (process.status) {
				case PS_DONE: 
					row.attr('class', 'success');
					break;
				case PS_EXECUTING: 
					row.attr('class', 'error');
					break;
				case PS_WAITING:
					row.attr('class', 'warning');
					break;
				default:
					row.attr('class', '');
				}
				$('#row_cpu_'+i).val(process.cpu);
				$('#row_waittime_'+i).html(process.waittime);
				row.children().eq(6).html(process.status); // nomainam statusa kolonnas vērtību
			} else {
				this.AddRow(i, process);
			}
		}
	}

	return Queue;
})();

var Chart = (function(){
	function Chart(animation, table){
		this.table = table;
		this.queue = null;
		this.animation = animation;
		this.width = 0;
		this.height = 0;		
		this.map = [];
		this.size = 0;
		this.table.html('');
	}	

	Chart.prototype.Clear = function(){
		this.map = [];
		this.size = 0;
		this.Init(this.width, this.height);
	}

	Chart.prototype.Init = function(w, h){
		this.table.html('');
		this.width = 0;
		this.height = 0;
				
		for (var i = 0; i < w; i++) 
			this.AddColumn();		

		this.AddHeader();
		for (var i = 0; i < h; i++) //h+1 jo vajag vēl tabulas galvu pievienot 
			this.AddRow();	
	}

	Chart.prototype.AddRow = function(n){
		//console.log('adding row...' + n);
		if (this.height == 0) this.AddHeader();

		if (n == undefined)
			n = 1;

		for (;n > 0; n--){						
			var row = $('<tr class="prow">');			
			for (var j = 0; j < this.width; j++) row.append($('<td>'));			
			this.table.append(row);		
			this.height++;	
		}
	};

	Chart.prototype.AddHeader = function(){		
		var row = $('<tr class="pheader">');
		for (var j = 0; j < this.width; j++){
			row.append($('<th>').html(j));
		}
		this.table.html(row);				
	}

	Chart.prototype.AddColumn = function(n){				
		//console.log('adding column...' + n);

		if (n == undefined)
			n = 1;

		if (n < 0) {
			this.RemoveColumn(-n);
		}

		for (;n > 0; n--){
			this.width++;			
			if (this.height == 0) continue;

			$('.pheader', this.table).append($('<th>').html(this.width-1))
			$('.prow', this.table).append($('<td>'));
		}

		this.UpdateTooltips();
	};

	Chart.prototype.RemoveColumn = function(n){
		if (n == undefined) n = 1;
		for (; n > 0; n--){
			$('.pheader', this.table).children().last().remove();
			$('.prow', this.table).each(function(i,e){ $(e).children().last().remove(); });
			this.width--;

		}
	};

	Chart.prototype.RemoveRow = function(y){
		if (y == undefined) y = this.height-1;
		if (this.height <= 0) return;
		this.height--;	
		$('.prow').eq(y).fadeOut('fast', function() { $(this).remove(); });
	};

	Chart.prototype.Get = function(x, y) {
		if (x < 0 || y < 0) return null;
		return $('.prow', this.table).eq(y).children().eq(x);
	}

	Chart.prototype.ProcessCreated = function(process){		
		if (process == null) return;

		console.log('Created process:');
		console.log(process);

		if (this.map[process.id] == undefined){
			this.map[process.id] = this.size++;
		}
		
		var tt = $('<div class="tt">');			
		tt.attr('data-placement','left');
		tt.attr('title', process.name);
		this.Get(this.animation.cpu.tick, this.map[process.id]).append(tt);
		tt.tooltip({'container': tt, trigger: 'manual'}).tooltip('show');
	}

	Chart.prototype.ProcessExecuted = function(process){
		if (process == null) return;

		console.log('Executed process:');
		console.log(process);		

		var bar = $('<div>').addClass('bar').attr('data-id', process.id).css('width', '100%').html(process.id);
		var cell = $('<div>').addClass('progress').append(bar);		

		this.Get(this.animation.cpu.tick-1, this.map[process.id]).append(cell);
		this.Update();
	}

	Chart.prototype.Update = function(){	
		this.AddColumn(Math.round(Math.max(this.queue.estimate * 1.25, this.animation.cpu.tick * 1.2)) - this.width);
		this.AddRow(this.queue.size - this.height);
		

		var ps = this.queue.processes;
		$('.bar', this.table).each(function (i, elem) {
			elem = $(elem);		
			var id = elem.attr('data-id');				
			switch (ps[id].status) {
			case PS_DONE: 
				elem.attr('class', 'bar bar-success');
				break;
			case PS_EXECUTING:
				elem.attr('class', 'bar bar-danger');
				break;
			case PS_WAITING:
				elem.attr('class', 'bar bar-warning');
				break;
			}			
		});	
	}

	Chart.prototype.UpdateTooltips = function(){
		$('.tt').each(function(i, elem){
	  		var tt = $(elem);
	  		tt.children().each(function (j, child) {
	  			var c = $(child);
	  			c.css({'left': (tt.offset().left - c.width()) + 'px'});	
	  		});
		});  
	}

	return Chart;
})();

init();
